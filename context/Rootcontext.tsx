import React, { createContext, useState } from "react";

export const RootContext: any = createContext({});

export const RootProvider: any = ({ children }) => {
  const [data, setData] = useState([
    {
      imgUrl: "https://picsum.photos/id/1005/200/200",
      place: "Amsterdam",
      details: "8pm, 12th August",
      id: "543ab333-60d7-40cb-8596-8e4325fec083",
    },
    {
      imgUrl: "https://picsum.photos/id/1003/200/200",
      place: "Paris",
      details: "8pm, 12th August",
      id: "5fbabd97-179e-48b9-bbab-5f01ee37c0a3",
    },
    {
      imgUrl: "https://picsum.photos/id/1001/200/200",
      place: "Berlin",
      details: "8pm, 12th August",
      id: "a1310051-7876-4a02-9ccc-6361100cd07b",
    },
    {
      imgUrl: "https://picsum.photos/id/1000/200/200",
      place: "Tokyo",
      details: "8pm, 12th August",
      id: "d7f4abae-a367-437a-a4b6-f88423c9df1f",
    },
    {
      imgUrl: "https://picsum.photos/id/100/200/200",
      place: "Manchester",
      details: "8pm, 12th August",
      id: "fb3425a8-6852-411a-a74f-43d0048a06bc",
    },
    {
      imgUrl: "https://picsum.photos/id/10/200/200",
      place: "Madrid",
      details: "8pm, 12th August",
      id: "7b217c35-9c76-4eb1-8133-05f5a513eb63",
    },
    {
      imgUrl: "https://picsum.photos/id/1012/200/200",
      place: "New York",
      details: "8pm, 12th August",
      id: "db045d5a-e53d-408b-b3c1-3410bbdb055d",
    },
    {
      imgUrl: "https://picsum.photos/id/1010/200/200",
      place: "Hamshir",
      details: "8pm, 12th August",
      id: "f9a9f2dd-57a0-46b1-ab27-fd609a5d6620",
    },
  ]);

  return (
    <RootContext.Provider
      value={{
        data,
        setData,
      }}
    >
      {children}
    </RootContext.Provider>
  );
};
