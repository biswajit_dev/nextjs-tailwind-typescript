import { Fragment, useContext } from "react";
import { RootContext } from "../context/Rootcontext";

import Header from "../components/Header";
import Layout from "../components/Layout";
import MeetupItem from "../components/MeetupItem";

const IndexPage = () => {
  const { data } = useContext(RootContext);

  return (
    <Fragment>
      <Header>
        <Layout>
          <div className="grid grid-cols-1 lg:grid-cols-4 md:grid-cols-3 sm:grid-cols-4 gap-4">
            {data.map(({ id, imgUrl, place, details }) => (
              <MeetupItem
                key={id}
                id={id}
                imgUrl={imgUrl}
                place={place}
                details={details}
              />
            ))}
          </div>
        </Layout>
      </Header>
    </Fragment>
  );
};

export default IndexPage;
