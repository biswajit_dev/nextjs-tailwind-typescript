import { useRouter } from "next/router";
import Image from "next/image";
import { useContext, useLayoutEffect, useState } from "react";

import { RootContext } from "../../context/Rootcontext";
import Header from "../../components/Header";
import Layout from "../../components/Layout";
import Loader from "../../components/Loader";

const PlaceDetails = () => {
  const { data } = useContext(RootContext);
  const [place, setPlace] = useState<any>({});
  const route = useRouter();
  useLayoutEffect(() => {
    if (!route.query.placeId) return;
    setPlace(data.find((el) => el.id === route.query.placeId));
  }, [data, route]);
  return (
    <Header>
      <Layout>
        {Object.keys(place).length === 0 ? (
          <Loader />
        ) : (
          <div className="w-60 h-80 pt-3 bg-gradient-to-br from-pink-400 to-red-400 rounded-lg shadow-2xl">
            <Image className="rounded-t-lg" src={place.imgUrl} width={200} height={200} />
            <h1>{place.place}</h1>
            <h4>{place.details}</h4>
          </div>
        )}
      </Layout>
    </Header>
  );
};

export default PlaceDetails;
