import React, { useContext } from "react";
import { useFormik } from "formik";
import { v4 as uuid_v4 } from "uuid";
import * as Yup from "yup";
import { useRouter } from "next/router";

import { RootContext } from "../context/Rootcontext";
import Layout from "../components/Layout";

const Addplace = () => {
  const { data, setData } = useContext(RootContext);
  const router = useRouter();

  const ValidationSchema = Yup.object().shape({
    imgUrl: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("*Image URL is Required"),
    place: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("*Place is Required"),
    details: Yup.string()
      .min(2, "Too Short!")
      .max(50, "Too Long!")
      .required("*Details are Required"),
  });

  const formik = useFormik({
    initialValues: {
      imgUrl: "",
      place: "",
      details: "",
    },
    validationSchema: ValidationSchema,
    onSubmit: (values) => {
      setData([
        ...data,
        {
          imgUrl: values.imgUrl,
          id: uuid_v4(),
          place: values.place,
          details: values.details,
        },
      ]);
      router.push("/");
    },
  });

  return (
    <Layout>
      <form onSubmit={formik.handleSubmit} className="flex flex-col text-left">
        <input
          className="border-solid border-2 border-blue-900 mt-4 p-1 h-8 rounded-lg"
          id="imgUrl"
          placeholder="Add Image URL"
          name="imgUrl"
          type="imgUrl"
          onChange={formik.handleChange}
          value={formik.values.imgUrl}
        />
        {formik.errors.imgUrl && (
          <div className="text-red-700">{formik.errors.imgUrl}</div>
        )}

        <input
          className="border-solid border-2 border-blue-900 mt-4 p-1 h-8 rounded-lg"
          id="place"
          placeholder="Add Place"
          name="place"
          type="place"
          onChange={formik.handleChange}
          value={formik.values.place}
        />
        {formik.errors.place && (
          <div className="text-red-700">{formik.errors.place}</div>
        )}

        <input
          className="border-solid border-2 border-blue-900 mt-4 p-1 h-8 rounded-lg"
          id="details"
          placeholder="Add Details"
          name="details"
          type="details"
          onChange={formik.handleChange}
          value={formik.values.details}
        />
        {formik.errors.details && (
          <div className="text-red-700">{formik.errors.details}</div>
        )}

        <button
          className="mt-4 w-32 h-8 rounded-3xl bg-gradient-to-br from-pink-400 to-red-600"
          type="submit"
        >
          Submit
        </button>
      </form>
    </Layout>
  );
};

export default Addplace;
