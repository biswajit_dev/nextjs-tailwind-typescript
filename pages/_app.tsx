import React from "react";
import { AppProps } from "next/app";

import { RootProvider } from "../context/Rootcontext";
import "../styles/index.css";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <RootProvider>
      <Component {...pageProps} />
    </RootProvider>
  );
}

export default MyApp;
