import { ReactNode } from "react";
import Head from "next/head";

interface Props {
  children: ReactNode;
}

const Layout = ({ children }: Props) => {
  return (
    <div className="mx-8 my-5">
      <Head>
        <title>Meetup🍺🍻</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <link rel="shortcut icon" href="../static/meetup.svg" />
      </Head>
      <div className="flex flex-col justify-center items-center text-center">
        {children}
      </div>
    </div>
  );
};

export default Layout;
