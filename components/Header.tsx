import Image from "next/image";
import { Fragment, ReactNode } from "react";
import Link from "next/link";
import AddImage from "../images/addimage.svg";

interface Props {
  children: ReactNode;
}

const Header = ({ children }: Props) => {
  return (
    <Fragment>
      <nav className="flex justify-between px-4 py-2 shadow-2xl">
        <h1 className="text-transparent bg-clip-text bg-gradient-to-br from-pink-400 to-red-600">
          Meetup Places
        </h1>
        <Link href="/addplace">
          <div className="flex items-center cursor-pointer">
            <Image src={AddImage} width={30} height={30} />
            <h5 className="ml-1">Add place</h5>
          </div>
        </Link>
      </nav>
      {children}
    </Fragment>
  );
};

export default Header;
