import Image from "next/image";
import Moreimg from "../images/more.svg";
import { useRouter } from "next/router";

interface Props {
    imgUrl: string,
    place: string,
    details: string,
    id: string
}

const MeetupItem = ({ imgUrl, place, details, id }: Props) => {
  const router = useRouter();
 
  return (
    <div className="shadow-lg h-72 w-52 rounded-lg bg-gradient-to-br from-pink-400 to-red-400 text-center">
      <img
        src={imgUrl}
        className="rounded-t-lg w-60 h-52 sm:w-60"
      />
      <div className="text-left p-2">
        <h4>{place}</h4>
        <h5>{details}</h5>
        <Image onClick={()=> router.push(`/placedetails/${id}`)} className="cursor-pointer" src={Moreimg} width={20} height={20} />
      </div>
    </div>
  );
};

export default MeetupItem;
